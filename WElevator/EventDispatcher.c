//
//  EventDispatcher.c
//  WElevator
//
//  Created by James Swineson on 12/16/15.
//  Copyright © 2015 James Swineson. All rights reserved.
//
#include "CommonInclude.h"
#include "EventDispatcher.h"

EventDispatchList *CreateEventDispatcherList(EventDict dict, int count)
{
	if (dict == NULL) return NULL;
	EventDispatchList *list = New(EventDispatchList);
	if (list == NULL) return NULL;
	list->totalTicks = 0;
	list->events = dict;
	list->eventTicks = NewArray(int, count);
	list->eventCount = count;
	for (int i = 0; i < list->eventCount; i++) list->eventTicks[i] = 0;
	return list;
}

EventDispatchList *TickEventDispatcherList(EventDispatchList *list)
{
    signal(SIGINT, SigintHandler);
	if (list == NULL) return NULL;
	list->totalTicks++;
	for (int i = 0; i < list->eventCount; i++)
	{
		Event *e = list->events + i;
		if (e->isEnabled && e->cycle != 0) list->eventTicks[i]++;
		if (e->cycle == list->eventTicks[i])
		{
			e->action(e, list->totalTicks / list->eventCount);
			list->eventTicks[i] %= e->cycle;
		}
	}
	return list;
}

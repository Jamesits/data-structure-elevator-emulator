//
//  Stack.h
//  WElevator
//
//  Created by James Swineson on 12/23/15.
//  Copyright © 2015 James Swineson. All rights reserved.
//
#pragma once
#ifndef Stack_h
#define Stack_h

#include "CommonInclude.h"
#define	STACK_INIT_SIZE	10	//存储空间初始分配量
#define	STACKINCREMENT	5	//存储空间分配增量
//乘客栈

typedef	 Client	*SElemType;
typedef struct ClientStack{
    SElemType	*base;		//栈底指针，栈不存在时base的值为NULL
    SElemType	*top;		//栈顶指针
    int			stacksize;	//当前已分配存储空间，以元素为单位
}ClientStack, *ClientStackp;

//乘客栈的基本操作
ClientStackp InitStack();
//构造一个空栈
bool DestroyStack(ClientStackp S);
//销毁栈S
bool ClearStack(ClientStackp S);
//把S置为空
bool StackEmpty(ClientStackp S);
//若栈S为空，则返回TRUE，否则返回FALSE
bool GetTop(ClientStackp S,SElemType e);
//返回栈顶元素
bool Push(ClientStackp S,SElemType e);
//入栈
SElemType Pop(ClientStackp S);
//出栈
int PrintStack(ClientStackp S);
//输出栈
void PrintStackInfo(ClientStackp S);

#endif /* Stack_h */

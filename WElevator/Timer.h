//
//  Timer.h
//  WElevator
//
//  Created by James Swineson on 12/16/15.
//  Copyright © 2015 James Swineson. All rights reserved.
//
#pragma once
#ifndef Timer_h
#define Timer_h
#include "CommonInclude.h"
extern long Now;

typedef struct TimerEvent{
    time_t time;
    char order[10];
    struct TimerEvent *next;
} TimerEvent;

extern TimerEvent *TopTimerEvent;

void PutTimerEvent(char *order, int time);

#endif /* Timer_h */

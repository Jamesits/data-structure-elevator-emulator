//
//  Elevator.h
//  WElevator
//
//  Created by James Swineson on 12/16/15.
//  Copyright © 2015 James Swineson. All rights reserved.
//
#pragma once
#ifndef Elevator_h
#define Elevator_h
#include "CommonInclude.h"
#include "PersonList.h"
#include "Timer.h"
#include "stack.h"

typedef struct Elevator{
    int floor;	//电梯所在层
    int ClientNumber;//电梯内人数
    EleStatus status;	//电梯当前状态
    EleStage Stage;	//电梯运行时期
    int Count;//用于电梯计时
    int CallUp[Maxfloor+1];//每层的Up按钮
    int CallDown[Maxfloor+1];//每层的Down按钮
    int CallCar[Maxfloor+1]; //电梯内的目标层按钮
    struct ClientStack *S[Maxfloor+1]; //乘客栈,要去不同楼层的人放在不同的栈中
    struct ClientStack *temp;
}Elevator, *Elevatorp;

extern Elevatorp Ele;

//电梯类型基本操作
Elevatorp InitEle(Elevatorp E);
Status CountOver(Elevatorp E);
void DestoryEle(Elevatorp E);
int EleFloor(Elevatorp E);
EleStatus ElevatorStatus(Elevatorp E);
Status RequireAbove(Elevatorp E);
Status RequireBelow(Elevatorp E);
Status EleAchieved(Elevatorp E);
Status EleOpenDoor(Elevatorp E);
Status ChangeEleCliFloor(Elevatorp E);

EleStage EleDecide(Elevatorp E);
Action ElevatorRun(Elevatorp E);
void InOut(Elevatorp E, WQueue *w[Maxfloor + 1][2]);
void NewClient(Elevatorp E, WQueue *w[Maxfloor+1][2]);

void Init();
void Quit();
#endif /* Elevator_h */

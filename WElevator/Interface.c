//
//  Interface.c
//  WElevator
//
//  Created by James Swineson on 12/16/15.
//  Copyright © 2015 James Swineson. All rights reserved.
//
#include "CommonInclude.h"
#include "Elevator.h"
#include "Events.h"

extern Action a;

#define BUF_SIZE 25
char *ElevatorOutputBuffer;

void clrscr()
{
#ifdef _WIN32
    system("@cls");
#else
    system("clear 2>null");
#endif
}

string getFloorName(int floor)
{
    static string floors[] =
    {
        "B",
        "1",
        "2",
        "3",
        "4",
    };
    return floors[floor];
}

char getElevatorStatusChar(Elevatorp E, Action a, int currentFloor) {
    if (E->floor != currentFloor) return '|';
    //输出电梯动作信息
    switch (a) {
        case DoorOpened:
            return ' ';
        case DoorClosed:
            return '|';
        case Achieved:
            return '_';
        case GoingUp:
            return '^';
        case GoingDown:
            return 'v';
        default:
            return ' ';
            break;
    };//switch
}

void PrintClientInfo(Clientp e, ClientStatus s) {
    //输出乘客信息
    switch (s) {
        case New:
            printf("%ld %d号乘客进入第%d层 忍耐时间为%ld 要去第%d层\n", Time, e->ClientID, e->Infloor, e->GiveUpTime, e->Outfloor);
            break;
        case GiveUp:
            printf("%ld %d号乘客放弃等待, 等待了\n", Time, e->ClientID);
            break;
        case Out:
            printf("%ld %d号乘客走出电梯\n", Time, e->ClientID);
            break;
        case In:
            printf("%ld %d号乘客走进电梯，要去第%d层\n", Time, e->ClientID, e->Outfloor);
            break;
        default:
            break;
    };
}

int CountStack(ClientStackp S)
{
    //输出栈
    int count=0;
    SElemType *i;
    i = S->base;
    while (i < S->top){
        printf("%d ", (*i++)->ClientID);
        count++;
    }
    return count;
}

void PrintElevatorStack(Elevatorp E, int floor, char *buf)
{
    ClientStackp S = E->S[floor];
    int ElevatorOutputBufferSize = 0;
    SElemType *i;
    i = S->base;
    while (i < S->top){
        if (ElevatorOutputBufferSize >= BUF_SIZE - 1) {
            sprintf(buf + ElevatorOutputBufferSize, "…");
            break;
        }
        ElevatorOutputBufferSize += sprintf(buf + ElevatorOutputBufferSize, "%2s", getFloorName((*i)->Outfloor));
        i++;
    }
    S = E->temp;
    if (!S) return;
    i = S->base;
    while (i < S->top){
        if (ElevatorOutputBufferSize >= BUF_SIZE - 1) {
            sprintf(buf + ElevatorOutputBufferSize, "…");
            break;
        }
        ElevatorOutputBufferSize += sprintf(buf + ElevatorOutputBufferSize, "%2s", getFloorName((*i)->Outfloor));
        i++;
    }
}

void PrintPeopleStack(ClientStackp S)
{
    //输出栈
    int count=0;
    SElemType *i;
    i = S->base;
    while (i < S->top){
        printf(" ID=%d OutFloor=%d", (*i)->ClientID, (*i)->Outfloor);
        i++;
        count++;
    }
}

void PrintPeopleQueue(WQueuep Q)
{
    //输出队列
    QueuePtr q;
    int count = 0;
    if (Q->front->next == NULL) return;
    q = Q->front->next;
    while (q != NULL) {
        //printf(" ID=%d OutFloor=%d GiveupTime=%ld ",q->data->ClientID, q->data->Outfloor, q->data->GiveUpTime);
        printf(" [%d]->%s", q->data->ClientID, getFloorName(q->data->Outfloor));
        q = q->next;
        count++;
    }
}

void PrintElevator(Elevatorp E, WQueue *w[5][2], Action a)
{
    // Print header
    printf("Floor\t Elevator\t\t    Queue\n");
    // Print floors
    for (int i = Maxfloor; i >= 0; i--) {
        // floor name
        printf("%s\t", getFloorName(i));
        // People in elevator
        if (!StackEmpty(E->S[i])) {
            ElevatorOutputBuffer = malloc(BUF_SIZE * sizeof(char));
            PrintElevatorStack(E, i, ElevatorOutputBuffer);
            printf("|%-24s ", ElevatorOutputBuffer);
            free(ElevatorOutputBuffer);
        } else {
            printf("|\t\t\t  ");
        }
        // Elevator status
        printf("%c", getElevatorStatusChar(E, a, i));
        // Waiting queue
        PrintPeopleQueue(w[i][Up]);
        PrintPeopleQueue(w[i][Down]);
        printf("\n");
    }
}

Action(RefreshUI)
{
#ifdef GUI
    clrscr();
    printf("Elevator Emulator\n");
    printf("Time: %d time unit, Total people: %d\n\n", totalTicks, ID);
    PrintElevator(E, w, a);
#endif
    return 0;
}

EventDict interfaceEventDict =
{
    {RefreshUI, 2, true},
};

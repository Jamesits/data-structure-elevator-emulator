﻿//
//  EventDispatcher.h
//  WElevator
//
//  Created by James Swineson on 12/16/15.
//  Copyright © 2015 James Swineson. All rights reserved.
//
#pragma once
#ifndef __EVENTDISPATCHER_H__
#define __EVENTDISPATCHER_H__
#include "CommonInclude.h"
#include "Timer.h"

// 事件处理函数签名
#define Action(X) int(X)(struct Event *actionEvent, int totalTicks)

// 事件触发器动作类型
typedef struct Event
{
	Action(*action);	// 触发的动作
	int cycle;			// 多少循环触发一次
	bool isEnabled;
} Event;

// 事件触发器动作列表类型
typedef Event EventDict[];

/*
用法：
	EventDict eventDict = {
	{action1, 1, true},
	{action2, 2, true},
	};
*/

typedef struct EventDispatchList 
{
	int eventCount;
	Event *events;
	int totalTicks;
	int *eventTicks;
} EventDispatchList;

EventDispatchList *CreateEventDispatcherList(EventDict dict, int count);
EventDispatchList *TickEventDispatcherList(EventDispatchList *list);
#endif
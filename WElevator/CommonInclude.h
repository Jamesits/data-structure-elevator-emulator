//
//  CommonInclude.h
//  WElevator
//
//  Created by James Swineson on 12/16/15.
//  Copyright © 2015 James Swineson. All rights reserved.
//
#pragma once
#ifndef __COMMONINCLUDE_H__
#define __COMMONINCLUDE_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#include <time.h>
#endif

// ===== Settings =====
// 1000000 is roughly 1s
#define CYCLE_DELAY 100000
#define MULTITHREAD
#define GUI
#define HEIGHT 5
// ===== Settings END =====

#ifdef MULTITHREAD
#include <pthread.h>
#endif

typedef char* string;
#define New(X) (X *)malloc(sizeof(X))
#define NewArray(X,Y) (X *)malloc((Y) * sizeof(X))
#define ArraySize(X) (sizeof(X) / sizeof((X)[0]))

//#define int long long
#include "Timer.h"
extern TimerEvent *event;
extern long Now;

typedef bool Status;

// SIGINT processing (in main.c)
extern volatile bool running;
void SigintHandler(int dummy);

//电梯状态
typedef enum {Opening, Opened, Closing, Closed, Moving, Decelerate, Waiting}EleStatus;
typedef enum {DoorOpened, DoorClosed, GoingUp, GoingDown, Achieved, None}Action;
typedef enum {Up, Down, OpenDoor, Stop}EleStage;
typedef enum {New, GiveUp, In, Out, Finish}ClientStatus;

#define	CloseTest 5	//电梯关门测试时间
#define OverTime  50	//电梯停候超时时间
#define Accelerate 5	//加速时间
#define UpTime	10	//上升时间
#define DownTime 10	//下降时间
#define UpDecelerate 10	//上升减速
#define DownDecelerate 10	//下降减速
#define DoorTime	5	//开门关门时间
#define InOutTime	5	//进出电梯时间
#define Maxfloor	4	//最高层
#define Minfloor	0	//最低层

extern time_t Time;	//时钟
extern time_t MaxTime;//系统运行最长时间
extern int InOutCount;//用于进出计时
extern time_t InterTime;//下一乘客进入系统的时间
extern int ID;		//乘客编号
extern int GiveUpNumber;//乘客放弃的数目
extern time_t TotalTime;//总共等待时间

#include "Elevator.h"
extern Elevatorp E;//电梯
extern WQueuep w[Maxfloor + 1][2];

#endif
//
//  PersonList.c
//  WElevator
//
//  Created by James Swineson on 12/16/15.
//  Copyright © 2015 James Swineson. All rights reserved.
//

#include "CommonInclude.h"
#include "PersonList.h"
#include "Elevator.h"

int ID = 0;
time_t Time = 0;
time_t InterTime = 0;
time_t TotalTime = 0;
int GiveUpNumber = 0;

Clientp CreatClient(Clientp p) {
    //生成新的乘客
    p = (Clientp) malloc(sizeof(Client));
    if (!p) return NULL;
    p->ClientID = ID++;
    p->GiveUpTime = rand() % 200 + 100;//产生所能容忍的等待时间
    p->InTime = Time;
    InterTime = rand() % 60;//产生下一乘客要到达的时间
    p->Outfloor = rand() % (Maxfloor + 1);    //产生所要到达的楼层
    //该乘客出现的楼层
    while ((p->Infloor = rand() % (Maxfloor + 1)) == p->Outfloor);
    //PrintClientInfo(p, New);
    return p;
}

Status DestoryClient(Clientp p) {
    //该乘客离开系统
    free(p);
    return EXIT_SUCCESS;
}

Status GoAbove(Clientp e) {
    //判断该乘客是否去往高层
    if (e->Outfloor > e->Infloor)
        return true;
    else
        return false;
}

int CInfloor(Clientp e) {
    //返回乘客进入的楼层
    return e->Infloor;
}

time_t CInTime(Clientp e) {
    //返回乘客进入时间
    return e->InTime;
}

int COutfloor(Clientp e) {
    //返回乘客进入时间
    return e->Outfloor;
}

WQueuep InitQueue(WQueuep Q) {
    //构造一个空队列Q
    Q=(WQueuep)malloc(sizeof(WQueue));
    Q->front = Q->rear = (QNode *) malloc(sizeof(QNode));
    if (!Q->front) return NULL;//分配存储失败
    Q->front->next = NULL;
    Q->front->data = NULL;
    Q->rear->next = NULL;
    return Q;
}

Status DestroyQueue(WQueuep Q) {
    //销毁队列Q
    while (Q->front) {
        Q->rear = Q->front->next;
        if (Q->front->data) DestoryClient(Q->front->data);
        free(Q->front);
        Q->front = Q->rear;
    }
    return EXIT_SUCCESS;
}

Status EnQueue(WQueuep Q, QElemType e) {
    //插入元素e为Q的新的队尾元素
    QueuePtr p;
    p = (QueuePtr) malloc(sizeof(QNode));
    if (!p) return NULL;
    p->data = e;
    p->next = NULL;
    Q->rear->next = (QNode*)p;
    Q->rear = p;
    return EXIT_SUCCESS;
}

QElemType DeQueue(WQueuep Q, QElemType e) {
    //若队列不空,则删除Q的队头元素,用e返回其值,并返回OK;
    //否则返回ERROR
    QueuePtr p;
    if (Q->front == Q->rear) return NULL;
    p = Q->front->next;
    e = p->data;
    Q->front->next = p->next;
    if (Q->rear == p)
        Q->rear = Q->front;
    p->next=NULL;
    free(p);
    return e;
}

Status QueueEmpty(WQueuep Q) {
    //判断队列是否为空
    if (Q->front == Q->rear) return true;
    else return false;
}

Status QDelNode(WQueuep Q, QueuePtr p) {
    //删除队列中p指向的结点的下一个结点
    QueuePtr q;
    if (p == NULL || p->next == NULL) return EXIT_FAILURE;
    q = p->next;
    p->next = q->next;
    if (p->next == NULL) Q->rear = p;
    DestoryClient(q->data);
    free(q);
    return EXIT_SUCCESS;
}

Status CGiveUp(WQueuep Q, int floor) {
    //删除放弃等待的乘客
    QueuePtr p;
    p = Q->front;
    if (p->next != NULL) {
        if (p->next->data->GiveUpTime == 0 && floor != p->next->data->Infloor) {
            //PrintClientInfo(p->next->data, GiveUp);
            TotalTime += Time - CInTime(p->next->data);
            QDelNode(Q, p);//将放弃等待的人删除
            GiveUpNumber++;
        } else {
            p->next->data->GiveUpTime--;
        }
    }
    
    return EXIT_SUCCESS;
}

void PrintQueue(WQueuep Q) {
    //输出队列
    QueuePtr q;
    int count = 0;
    if (Q->front->next == NULL) return;
    q = Q->front->next;
    while (q != NULL) {
        printf(" ID=%d OutFloor=%d GiveupTime=%ld ",q->data->ClientID, q->data->Outfloor, q->data->GiveUpTime);
        q = q->next;
        count++;
    }
}

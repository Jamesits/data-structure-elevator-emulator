//
//  Timer.c
//  WElevator
//
//  Created by James Swineson on 12/16/15.
//  Copyright © 2015 James Swineson. All rights reserved.
//

#include "CommonInclude.h"
#include "Timer.h"

TimerEvent *TopTimerEvent;

void PutTimerEvent(char *order, int time) {
    TimerEvent *insterTimerEvent;
    time += Now;
    insterTimerEvent = (TimerEvent *)malloc(sizeof(TimerEvent));
    strcpy(insterTimerEvent->order, order);
    insterTimerEvent->time = time;
    TimerEvent *beforeTimerEvent = TopTimerEvent;
    TimerEvent *afterTimerEvent = TopTimerEvent->next;
    // 找到任务注册点
    
    while (time >= afterTimerEvent->time) {
        if (afterTimerEvent->next == NULL) break;
        beforeTimerEvent = afterTimerEvent;
        afterTimerEvent = afterTimerEvent->next;
        
        
    }
    if (time <= afterTimerEvent->time) {
        
        beforeTimerEvent->next = insterTimerEvent;
        insterTimerEvent->next = afterTimerEvent;
        
    }
    
    //
    // 扔掉 大于 MaxTime的时间
}
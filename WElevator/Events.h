//
//  Events.h
//  WElevator
//
//  Created by James Swineson on 12/16/15.
//  Copyright © 2015 James Swineson. All rights reserved.
//
#pragma once
#ifndef __EVENTS_H__
#define __EVENTS_H__
#include "CommonInclude.h"
#include "EventDispatcher.h"

Action(ClockAction);
Action(TimerTick);

extern EventDict elevatorEventDict;
extern EventDict interfaceEventDict;

#endif
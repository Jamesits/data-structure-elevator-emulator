//
//  PersonList.h
//  WElevator
//
//  Created by James Swineson on 12/16/15.
//  Copyright © 2015 James Swineson. All rights reserved.
//
#pragma once
#ifndef PersonList_h
#define PersonList_h
#include "CommonInclude.h"
#include "Events.h"
#include "Timer.h"

//乘客类型
typedef struct Client{
    int ClientID;	//乘客编号
    int Outfloor;	//去哪层
    time_t InTime;	//该乘客进入时间
    time_t GiveUpTime;	//所能容忍的等待时间
    int Infloor;//乘客进入的楼层
}Client, *Clientp;

//乘客类型基本操作
//void PrintClientInfo(Clientp e, ClientStatus s);
Clientp CreatClient(Clientp p);
Status DestoryClient(Clientp p);
Status GoAbove(Clientp e);
int CInfloor(Clientp e);
time_t CInTime(Clientp e);
int COutfloor(Clientp e);

typedef Client *QElemType;
//等候队列
typedef struct QNode {
    QElemType		data;
    struct QNode	*next;
}QNode,*QueuePtr;

typedef struct WQueue{
    QueuePtr	front;	//队头指针
    QueuePtr	rear;	//队尾指针
}WQueue, *WQueuep;
//等待队列的基本操作
WQueuep InitQueue(WQueuep Q);
Status DestroyQueue(WQueuep Q);
Status EnQueue(WQueuep Q, QElemType e);
QElemType DeQueue(WQueuep Q, QElemType e);
Status QueueEmpty(WQueuep Q);
Status QDelNode(WQueuep Q, QueuePtr p);
Status CGiveUp(WQueuep Q, int floor);
void PrintQueue(WQueuep Q);
#endif /* PersonList_h */

//
//  main.c
//  WElevator
//
//  Created by James Swineson on 12/16/15.
//  Copyright © 2015 James Swineson. All rights reserved.
//
#define _CRT_SECURE_NO_WARNINGS
#include "CommonInclude.h"
#include "Elevator.h"
#include "Events.h"
#include "EventDispatcher.h"
#include <ctype.h>
#include <limits.h>
#include <signal.h>

volatile bool running = true;

void SigintHandler(int dummy)
{
    running = false;
}

void waitThread()
{
#ifdef _WIN32
    Sleep(CYCLE_DELAY);
#else
    usleep(CYCLE_DELAY);
    struct timespec t = {0, CYCLE_DELAY};
    nanosleep(&t, NULL);
#endif
}

TimerEvent *event;
long Now = 0;
Elevatorp E;
time_t MaxTime  = LONG_MAX;
WQueuep w[Maxfloor + 1][2];

int main() {
    signal(SIGINT, SigintHandler);
    srand((unsigned) time(NULL));
    Init();
    event = TopTimerEvent;
    //printf(" \nBegin\n -----------------------------\n");
    EventDispatchList *elevatorEmulator = CreateEventDispatcherList(elevatorEventDict, 2);
    EventDispatchList *ui = CreateEventDispatcherList(interfaceEventDict, 1);

    while (running)
    {
#ifdef MULTITHREAD
        pthread_t thread_emu;
        pthread_t thread_ui;
        pthread_t thread_timer;
        pthread_create(&thread_emu, NULL, (void*(*)(void*))TickEventDispatcherList, elevatorEmulator);
        pthread_create(&thread_ui, NULL, (void*(*)(void*))TickEventDispatcherList, ui);
        pthread_create(&thread_timer, NULL, (void*(*)(void*))waitThread, NULL);
        pthread_join(thread_ui, NULL);
        pthread_join(thread_emu, NULL);
        pthread_join(thread_timer, NULL);
        pthread_detach(thread_ui);
        pthread_detach(thread_emu);
        pthread_detach(thread_timer);
#else
        TickEventDispatcherList(elevatorEmulator);
        TickEventDispatcherList(ui);
        waitThread();
#endif
    }
    Quit();
    return 0;
}

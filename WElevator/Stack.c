//
//  Stack.c
//  WElevator
//
//  Created by James Swineson on 12/23/15.
//  Copyright © 2015 James Swineson. All rights reserved.
//

#include "Stack.h"

ClientStackp InitStack() {
    ClientStackp S;
    //构造一个空栈
    S=(ClientStackp)malloc(sizeof(ClientStack));
    S->base = (SElemType *) malloc(STACK_INIT_SIZE * sizeof(Client));
    if (!S->base) return NULL;
    S->top = S->base;
    S->stacksize = STACK_INIT_SIZE;
    return S;
}//InitStack

bool DestroyStack(ClientStackp S) {
    //销毁栈S
    SElemType *p;
    if (S->base) {
        for (p = S->base; p < S->top; p++)
            DestoryClient(*p);
        free(S->base);
    }
    return EXIT_SUCCESS;
}

bool ClearStack(ClientStackp S) {
    //把S置为空
    if (!S->base) return EXIT_FAILURE;
    S->top = S->base;
    return EXIT_SUCCESS;
}

bool StackEmpty(ClientStackp S) {
    //若栈S为空，则返回TRUE，否则返回FALSE
    if (S->top == S->base) return true;
    else return false;
}

bool GetTop(ClientStackp S, SElemType e) {
    //返回栈顶元素
    if (!S->base) return EXIT_FAILURE;
    e = *(S->top - 1);
    return EXIT_SUCCESS;
}//GetTop

bool Push(ClientStackp S, SElemType e) {
    //入栈
    if (!S->base) return EXIT_FAILURE;
    if (S->top - S->base >= S->stacksize) {
        S->base = (SElemType *) realloc(S->base, (S->stacksize + STACKINCREMENT) * sizeof(SElemType));
        if (!S->base) return NULL;
        S->top = S->base + S->stacksize;
        S->stacksize += STACKINCREMENT;
    }
    *S->top++ = e;
    return EXIT_SUCCESS;
}//Push

SElemType Pop(ClientStackp S) {
    //出栈
    SElemType e;
    if (S->top == S->base) return NULL;
    e = *(--S->top);
    return e;
}//Pop

int PrintStack(ClientStackp S) {
    //输出栈
    int count=0;
    SElemType *i;
    i = S->base;
    while (i < S->top){
        printf("%d ", (*i++)->ClientID);
        count++;
    }
    return count;
}

void PrintStackInfo(ClientStackp S) {
    //输出栈
    int count=0;
    SElemType *i;
    i = S->base;
    while (i < S->top){
        printf(" ID=%d OutFloor=%d \n", (*(i))->ClientID, (*i)->Outfloor);
        i++;
        count++;
    }
}
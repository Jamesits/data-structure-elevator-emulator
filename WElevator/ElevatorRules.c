﻿//
//  ElevatorRules.c
//  WElevator
//
//  Created by James Swineson on 12/16/15.
//  Copyright © 2015 James Swineson. All rights reserved.
//
#include "CommonInclude.h"
#include "Events.h"
#include "Timer.h"
#include "Elevator.h"

Action a;

Action(ClockAction)
{
    Now++;
    return 0;
}

Action(TimerTick)
{
    if (InterTime == 0) NewClient(E, w);
    else InterTime--;
    for (int i = 0; i <= Maxfloor; i++)
        for (int j = 0; j < 2; j++) {
            CGiveUp(w[i][j], EleFloor(E));
        }
    
    if (InOutCount == 0) {
        if (ElevatorStatus(E) == Opened) InOut(E, w);
    }
    else InOutCount--;
    
    
    if (CountOver(E) || ElevatorStatus(E) == Closed || ElevatorStatus(E) == Waiting) {
        a = ElevatorRun(E);
    }
    return 0;
};

EventDict elevatorEventDict =
{
    { ClockAction, 1, true },
    { TimerTick, 1, true },
};

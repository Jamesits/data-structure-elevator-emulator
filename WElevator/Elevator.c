//
//  Elevator.c
//  WElevator
//
//  Created by James Swineson on 12/16/15.
//  Copyright © 2015 James Swineson. All rights reserved.
//

#include "CommonInclude.h"
#include "Elevator.h"

int InOutCount = 0;

void Init()
{
    E = InitEle(E);
    int i;
    for (i = 0; i <= Maxfloor; i++) {
        *w[i] = (WQueue *) malloc(2 * sizeof(WQueue));
    }
    for (i = 0; i <= Maxfloor; i++) {
        w[i][Up] = InitQueue(w[i][Up]);
        w[i][Down] = InitQueue(w[i][Down]);
    }
}

void Quit()
{
    printf("\n");
    printf("Total people: %d\n", ID);
    printf("Give-up times: %d\n", GiveUpNumber);
    //printf("Average waiting time: %f\n", (float)TotalTime / ID);
    
    DestoryEle(E);
    for (int i = 0; i <= Maxfloor; i++) {
        DestroyQueue(w[i][Up]);
        DestroyQueue(w[i][Down]);
    }
}

Elevatorp InitEle(Elevatorp E) {
    //初始化电梯类型
    E = (Elevatorp) malloc(sizeof(Elevator));
    int i;
    E->floor = 1;//电梯初始停在第一层
    E->status = Waiting;
    E->Count = OverTime;
    E->Stage = Down;
    E->ClientNumber = 0;
    for (i = 0; i <= Maxfloor; i++) {
        E->CallUp[i] = 0;
        E->CallDown[i] = 0;
        E->CallCar[i] = 0;
    }
    for (i = 0; i <= Maxfloor; i++)
        E->S[i] = InitStack(E->S[i]);
    return E;
}

Status CountOver(Elevatorp E) {
    //判断电梯计时是否完成
    if (E->Count) {
        E->Count--;
        return false;
    }
    return true;
}

void DestoryEle(Elevatorp E) {
    //销毁电梯类型
    int i;
    for (i = 0; i <= Maxfloor; i++)
        DestroyStack(E->S[i]);
}

int EleFloor(Elevatorp E) {
    //返回电梯所在的层
    return E->floor;
}

EleStatus ElevatorStatus(Elevatorp E) {
    //返回电梯状态
    return E->status;
}

Status RequireAbove(Elevatorp E) {
    //判断是否有高层请求
    int i;
    for (i = E->floor + 1; i <= Maxfloor; i++)
        if (E->CallCar[i] || E->CallDown[i] || E->CallUp[i])
            return true;
    return false;
}

Status RequireBelow(Elevatorp E) {
    //判断是否有低层请求
    int i;
    for (i = E->floor - 1; i >= Minfloor; i--)
        if (E->CallCar[i] || E->CallDown[i] || E->CallUp[i])
            return true;
    return false;
}

Status EleAchieved(Elevatorp E) {
    //判断电梯是否要停于当前层
    if (E->CallCar[E->floor])
        return true;
    if ((E->Stage == Up && E->CallUp[E->floor]) || (E->Stage == Down && E->CallDown[E->floor]))
        return true;
    if (E->Stage == Up && E->CallDown[E->floor] && !RequireAbove(E)) {
        E->Stage = Down;
        return true;
    }
    if (E->Stage == Down && E->CallUp[E->floor] && !RequireBelow(E)) {
        E->Stage = Up;
        return true;
    }
    return false;
}

Status EleOpenDoor(Elevatorp E) {
    //判断电梯是否要开门
    if (E->CallCar[E->floor] || (E->CallDown[E->floor] && E->Stage == Down) || (E->CallUp[E->floor] && E->Stage == Up))
        return true;
    if (E->status == Waiting) {
        if (E->CallDown[E->floor]) {
            E->Stage = Down;
            return true;
        }
        if (E->CallUp[E->floor]) {
            E->Stage = Up;
            return true;
        }
    }
    return false;
}

//*******************************************************************
EleStage EleDecide(Elevatorp E) {
    //判断电梯动作
    int Above, Below;
    Above = RequireAbove(E);
    Below = RequireBelow(E);
    //无请求则停止
    if (Above == 0 && Below == 0)
        return Stop;
    //有请求则按请求移动
    else {
        if (E->Stage == Up) {
            if (Above != 0)
                return Up;
            else {
                E->Stage = Down;
                return Down;
            }
        }//if
        else {
            if (Below != 0)
                return Down;
            else {
                E->Stage = Up;
                return Up;
            }
        }//if
    }
}

Action ElevatorRun(Elevatorp E) {
    //电梯状态转换
    switch (E->status) {
        case Opening:
            //完成开门则转入Opened状态
            E->status = Opened;
            E->Count = CloseTest;
            return DoorOpened;
        case Opened:
            //进行关门测试
            if ((E->Stage == Down && !E->CallCar[E->floor] && !E->CallDown[E->floor]) ||
                (E->Stage == Up && !E->CallCar[E->floor] && !E->CallUp[E->floor])) {//无人进出，关门
                E->status = Closing;
                E->Count = DoorTime;
            }//if
            break;
        case Closing:
            //完成关门则转入Closed状态
            E->status = Closed;
            return DoorClosed;
        case Waiting:
            //不在第一层且超出所规定的停候时间,电梯向第一层移动
            if (E->Count == 0) {
                if (E->floor != 1) E->CallCar[1] = 1;
            }
            else E->Count--;
            //如果有人可以进入，则开门
            if (EleOpenDoor(E)) {
                E->status = Opening;
                E->Count = DoorTime;
                break;
            }
        case Closed:
            //根据EleDecide的返回值设定电梯状态
            switch (EleDecide(E)) {
                case Up:
                    E->status = Moving;
                    E->Count = UpTime + Accelerate;
                    return GoingUp;
                case Down:
                    E->status = Moving;
                    E->Count = DownTime + Accelerate;
                    return GoingDown;
                case Stop:
                    if (E->status != Waiting) {
                        E->status = Waiting;
                        E->Count = OverTime;
                    }
                case OpenDoor:
                    break;
            };//switch
            break;
        case Moving:
            //完成移动
            if (E->Stage == Up) {
                E->floor++;
                if (!StackEmpty(E->S[E->floor - 1]))
                    ChangeEleCliFloor(E);
            } else {
                E->floor--;
                if (!StackEmpty(E->S[E->floor + 1]))
                    ChangeEleCliFloor(E);
            }
            if (EleAchieved(E)) {
                //到达目标层，转入减速状态
                E->status = Decelerate;
                E->Count = DownDecelerate;
            }
            else E->Count += DownTime;
            //未到达目标层，继续下降
            return Achieved;
        case Decelerate:
            //完成减速
            //确定正确的电梯时期
            if (E->Stage == Up && !E->CallUp[E->floor] && !RequireAbove(E)) E->Stage = Down;
            else if (E->Stage == Down && !E->CallDown[E->floor] && !RequireBelow(E)) E->Stage = Up;
            //转到开门状态
            E->status = Opening;
            E->Count = DoorTime;
            break;
    };//switch
    return None;
}//ElevatorRun

void InOut(Elevatorp E, WQueue *w[Maxfloor + 1][2]) {
    //进行乘客的进出电梯活动
    // disable UI refresh
    interfaceEventDict[0].isEnabled = false;
    Client *p = NULL;
    static ClientStackp GunHuiElevatorLiMianLiangKuaiQu;
    if (E->CallCar[E->floor]) {//人要从电梯中走出
        if (StackEmpty(E->S[E->floor])) {
            while(!StackEmpty(GunHuiElevatorLiMianLiangKuaiQu)) {
                p = Pop(GunHuiElevatorLiMianLiangKuaiQu);
                Push(E->S[E->floor], p);
            }
            DestroyStack(GunHuiElevatorLiMianLiangKuaiQu);
            GunHuiElevatorLiMianLiangKuaiQu = NULL;
            E->CallCar[E->floor] = 0;
        }
        else {
            if (GunHuiElevatorLiMianLiangKuaiQu == NULL) GunHuiElevatorLiMianLiangKuaiQu = InitStack();
            if (!StackEmpty(E->S[E->floor])) {
            //当前层的乘客栈非空，出电梯
                p = Pop(E->S[E->floor]);
                if (p->Outfloor == E->floor) {
                    E->ClientNumber--;
                    InOutCount = InOutTime;
                    //printf(" Kill: %d from %d to %d\n", p->ClientID, p->Infloor, p->Outfloor);
                    TotalTime += Time - CInTime(p);
                    DestoryClient(p);
                } else {
                    Push(GunHuiElevatorLiMianLiangKuaiQu, p);
                    //printf(" Pull: "); PrintStack(GunHuiElevatorLiMianLiangKuaiQu);
                }
            }
        }
    }
    if (E->CallCar[E->floor] == 0) {//有人要走入电梯
        if (!QueueEmpty(w[E->floor][E->Stage])) {
            //若队列不空，继续进电梯
            p = DeQueue(w[E->floor][E->Stage], p);
            Push(E->S[CInfloor(p)], p);
            if (E->CallCar[COutfloor(p)] != 1) {
                //按下要去楼层的按钮
                E->CallCar[COutfloor(p)] = 1;
            }
            E->ClientNumber++;
            InOutCount = InOutTime;
            // PrintClientInfo(p, In);
        }//if
        else {
            //乘客的进出活动已完成
            if (E->Stage == Down) E->CallDown[E->floor] = 0;//将相应的下降按钮取消
            else E->CallUp[E->floor] = 0;//将相应的上升按钮取消
        }
    }
    E->temp = GunHuiElevatorLiMianLiangKuaiQu;
    // enable UI refresh again
    interfaceEventDict[0].isEnabled = true;
}

void NewClient(Elevatorp E, WQueue *w[Maxfloor + 1][2]) {
    //进入新乘客
    Clientp p = NULL;
    p = CreatClient(p);//新的乘客
    //将该乘客插入相应队列并按下相应按钮(Up/Down)
    if (GoAbove(p)) {
        EnQueue(w[CInfloor(p)][Up], p);
        E->CallUp[CInfloor(p)] = 1;
    }
    else {
        EnQueue(w[CInfloor(p)][Down], p);
        E->CallDown[CInfloor(p)] = 1;
    }//else
}

Status ChangeEleCliFloor(Elevatorp E) {
    ClientStackp p;
    int floor = E->floor;
    p = E->S[floor];
    if (E->Stage == Up) {
        p = E->S[floor];
        E->S[floor] = E->S[floor - 1];
        E->S[floor - 1] = p;
    } else {
        p = E->S[floor];
        E->S[floor] = E->S[floor + 1];
        E->S[floor + 1] = p;
    }
    return false;
}
